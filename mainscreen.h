#ifndef MAINSCREEN_H
#define MAINSCREEN_H

#include <QMainWindow>
#include "patientwindow.h"
#include "zakrokywindow.h"
#include <QMessageBox>
#include "databaseagent.h"
#include "tablefields.h"
#include <QSqlQueryModel>
#include <QSqlTableModel>

namespace Ui {
class MainScreen;
}

class MainScreen : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainScreen(QWidget *parent = 0);
    ~MainScreen();


    unsigned int currentId = 0;



    void initiateDB();

    unsigned int getCurrentIdInt() const;
     QString getCurrentId() const;

public slots:
    void dbRefresh();

private slots:
    void on_buttonNewPatient_clicked();

    void on_buttonArchivatePatient_clicked();

    void on_buttonEditPatient_clicked();

    void on_buttonNewExamination_clicked();

    void rowChanged();

    void on_actionSpravovat_z_kroky_triggered();

    void on_calendarWidget_selectionChanged();
    void showContextMenu(const QPoint &pos);

signals:
    void createNewPatient();
    void editPatient(QString id);
    void dbUpdated();

private:
    Ui::MainScreen *ui;
    PatientWindow *pw;
    ZakrokyWindow *zw;
    QSqlQueryModel *model;
    //QMenu *calContextMenu;


    void updateFlags();
    void initiateModel();
    void newPatient();
    void editPatient();
};

#endif // MAINSCREEN_H
