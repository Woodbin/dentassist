#include "mainscreen.h"
#include "ui_mainscreen.h"
#include "patientwindow.h"

MainScreen::MainScreen(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainScreen)
{
    DatabaseAgent::getInstance();

    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
            this, SLOT(showContextMenu(const QPoint &)));
    ui->setupUi(this);
    initiateModel();

    pw = new PatientWindow(this);
    pw->hide();
}

MainScreen::~MainScreen()
{
    delete ui;
}

unsigned int MainScreen::getCurrentIdInt() const
{
    return currentId;
}


QString MainScreen::getCurrentId() const
{
    return QString::number(getCurrentIdInt());

}
void MainScreen::initiateDB()
{
    
}

void MainScreen::on_buttonNewPatient_clicked()
{



    pw->show();
    pw->populateEmpty();
//    connect(this, SIGNAL(createNewPatient()),
//                       pw, SLOT(populateEmpty()));

    connect(pw, SIGNAL(patientAdded()),this, SLOT(dbRefresh()));

}

void MainScreen::on_buttonArchivatePatient_clicked()
{
    QMessageBox::StandardButton reply;
     reply = QMessageBox::question(this, "Archivace pacienta", "Chcete skutečně archivovat pacienta?",
                                   QMessageBox::Yes|QMessageBox::No);
     if (reply == QMessageBox::Yes) {

     } else {

     }
}

void MainScreen::on_buttonEditPatient_clicked()
{


        pw = new PatientWindow(this);
        pw->setParent(this);
    pw->show();
    pw->populateEdit(getCurrentId());
    pw->lockFields();
}

void MainScreen::on_buttonNewExamination_clicked()
{

}

void MainScreen::initiateModel()
{




//    model =  new QSqlTableModel(ui->tableView, DatabaseAgent::getInstance().db);
    model =  new QSqlTableModel();


    //model->setTable(DatabaseAgent::getInstance().getTABLE_PACIENTI());

    model->setHeaderData(Pacienti_id,Qt::Horizontal, QObject::tr("ID"));
    model->setHeaderData(Pacienti_Prijmeni,Qt::Horizontal, QObject::tr("Příjmení"));
    model->setHeaderData(Pacienti_Jmeno,Qt::Horizontal, QObject::tr("Jméno"));
    model->setHeaderData(Pacienti_RC,Qt::Horizontal, QObject::tr("Rodné číslo"));
    model->setHeaderData(Pacienti_Flags,Qt::Horizontal,QObject::tr("Příznaky"));
    model->setHeaderData(Pacienti_Telefon,Qt::Horizontal, QObject::tr("Telefonní číslo"));

    model->setQuery(DatabaseAgent::getInstance().getSELECT_PATIENTS_VIEW());



    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView->setColumnHidden(Pacienti_id, true);
    ui->tableView->setColumnHidden(Pacienti_Flags, true);
    ui->tableView->resizeColumnsToContents();
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);


    ui->tableView->setModel(model);

    QHeaderView *header = ui->tableView->horizontalHeader();
    header->setStretchLastSection(true);

    QItemSelectionModel *select = ui->tableView->selectionModel();
    connect(select, SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(rowChanged()));

    ui->tableView->show();
}

void MainScreen::rowChanged()
{
    currentId = ui->tableView->selectionModel()->currentIndex().row();
    qDebug() << "Row changed! id: " << currentId;
    pw->setCurrentId(currentId);
    updateFlags();
}

void MainScreen::updateFlags()
{
    if(currentId!=0)
    {
       qDebug() << "Updating flags with data from patient " << currentId << " and flags " << model->index(currentId,Pacienti_Flags).data().toString();
       const char* haystack = model->index(currentId,Pacienti_Flags).data().toString().toStdString().c_str();

       ui->labelAstmatik->setEnabled(strstr(haystack,"a"));
       ui->labelHIV->setEnabled(strstr(haystack,"h"));
       ui->labelKardiak->setEnabled(strstr(haystack,"k"));
       ui->labelOther->setEnabled(strstr(haystack,"j"));
    }
}

void MainScreen::dbRefresh()
{
    model->setQuery(DatabaseAgent::getInstance().getSELECT_PATIENTS_VIEW());
     ui->tableView->setModel(model);
}

void MainScreen::on_actionSpravovat_z_kroky_triggered()
{
    zw = new ZakrokyWindow(this);
    zw->show();
}

void MainScreen::on_calendarWidget_selectionChanged()
{
    qDebug()<< "calendar click";


}

void MainScreen::showContextMenu(const QPoint &pos)
{
    if(!ui->tableView->selectionModel()->selectedIndexes().isEmpty())
    {
        QMenu calContextMenu(tr("Kontextová nabídka"),this);
        QAction newAppointment("Nová schůzka...",this);
        connect(&newAppointment, SIGNAL(triggered()), pw, SLOT(newAppointment(currentId)));
        calContextMenu.addAction(&newAppointment);
        calContextMenu.exec(mapToGlobal(pos));
    }
}

