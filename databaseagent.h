#ifndef DATABASEAGENT_H
#define DATABASEAGENT_H

#include <QObject>  //TODO Check the format is right
#include <QtSql>
#include <QVector>
#include <QtDebug>
#include <iostream>
#include <string>
#include <sstream>
#include <QMessageBox>
#include "pacient.h"

class DatabaseAgent
{
    public:         //TODO add public methods

        QSqlDatabase db; //= new QSqlDatabase();
        QSqlTableModel *model;
        const QString TABLE_PACIENTI = "pacienti";
        const QString TABLE_PROHLIDKA = "prohlidka";
        const QString TABLE_PROHLIDKA_ZAKROKY = "prohlidkaZakrok";
        const QString TABLE_ZAKROKY = "zakroky";
        const QString TABLE_META = "meta";
        //TODO Udělat tabulku na zdravotní stavy a jiné diagnozy - s informacemi o léčbě, na co nezapomenout apod

        const QString COLUMN_PROHLIDKA_ID = "id";
        const QString COLUMN_PROHLIDKA_ID_PACIENT = "pacient_id";
        const QString COLUMN_PROHLIDKA_DATE = "datum_schuzky";
        const QString COLUMN_PROHLIDKA_ID_ZAKROKY = "zakrok_id";        //TODO FIX Nejspíš zbytečné
        const QString COLUMN_PROHLIDKA_NOTES = "poznamky";

        const QString COLUMN_PROHLIDKA_ZAKROKY_ID = "id";
        const QString COLUMN_PROHLIDKA_ZAKROKY_ID_PROHLIDKA = "prohlidka_id";
        const QString COLUMN_PROHLIDKA_ZAKROKY_ID_ZAKROK = "zakrok_id";

        const QString COLUMN_ZAKROKY_ID = "id";
        const QString COLUMN_ZAKROKY_ZAKROK = "popis";
        const QString COLUMN_ZAKROKY_CENA= "cena";

        const QString COLUMN_PACIENT_ID = "id";
        const QString COLUMN_PACIENT_JMENO = "jmeno";
        const QString COLUMN_PACIENT_PRIJMENI = "prijmeni";
        const QString COLUMN_PACIENT_RC = "rodnec";
        const QString COLUMN_PACIENT_CP = "cislopojistence";
        const QString COLUMN_PACIENT_FLAGS = "znacky";
        const QString COLUMN_PACIENT_IBOB = "ibob";
        const QString COLUMN_PACIENT_PBI = "pbi";
        const QString COLUMN_PACIENT_DKEY = "zubniklic";
        const QString COLUMN_PACIENT_PARO = "parodontoza";
        const QString COLUMN_PACIENT_STONE = "zubnikamen";
        const QString COLUMN_PACIENT_ADRESA = "adresa";
        const QString COLUMN_PACIENT_NOTES = "poznamky";
        const QString COLUMN_PACIENT_DOKTOR = "doktor";
        const QString COLUMN_PACIENT_TELEFON = "telefon";

        const QString COLUMN_META_ID = "id";
        const QString COLUMN_META_TAG = "tag";
        const QString COLUMN_META_CONTENT = "content";

        void archivatePatient(int id);



            static DatabaseAgent& getInstance()
            {
                static DatabaseAgent    instance; // Guaranteed to be destroyed.
                                      // Instantiated on first use.
                return instance;
            }

            //void insertNewPatient(QString name, QString surname, QString dateOfBirth, QString bornNum, QString adress, QString insuranceNum="");
            void insertNewPatient(QString name, QString surname, QString bornNum,  QString adress, QString insuranceNum="",
                                  QString phone = "", QString caretaker = "", QString flags="", QString notes=""  );








            QString getTABLE_PACIENTI() const;

            QString getTABLE_PROHLIDKA() const;

            QString getTABLE_PROHLIDKA_ZAKROKY() const;

            QString getTABLE_ZAKROKY() const;

            QString getTABLE_META() const;

            QString getSELECT_PATIENTS_VIEW() const;
            QString getSELECT_ZAKROKY_VIEW() const;

            void insertZakrok(QString zakrok, QString cena);
            int insertNewProhlidka(QString date, QString idPacient, QString notes, QString idZakroky);
            void insertZakrokyProhlidky(QStringList zakroky, QString idprohlidky);
            void deleteZakrok(QString id);
            void upravZakrok(QString id, QString zakrok, QString cena);
            void getProhlidky(QString patientId);
            void getProhlidka(QString prohlidkaId);
            pacient getPacient(QString pacientId);

            QString getSELECT_PROHLIDKY_VIEW(QString patientId);

            QString getSELECT_PROHLIDKA_VIEW(QString patientId) ;

            QString getINSERT_PROHLIDKA(QString date, QString idPacient, QString notes, QString idZakroky) ;

            QString getINSERT_ZAKROKY_PROHLIDKY(QString idProhlidka, QString idZakrok);

            QString getSELECT_PACIENT(QString pacientId) const;

private:        //TODO add private methods
            DatabaseAgent() {

//                QString path = "./dentassist.db";
                QString path = QCoreApplication::applicationDirPath()+"dentassist.db";

                db = QSqlDatabase::addDatabase("QSQLITE");//not dbConnection
                db.setDatabaseName(path);
                if (!db.open()) {
                    qDebug() << "DB Opening error";
                    QMessageBox::critical(0, QObject::tr("Chyba databáze"),
                                                     db.lastError().text());
                           }else{
                    qDebug() << "DB Succesfully opened:";
                    createTables();
                    insertTestData();
                }


            }

            void createTables();
            void insertTestData();



            const QString DB_NAME = "DentAssist";
            const QString DB_FILE = DB_NAME+".db";

            const QString SELECT_PACIENT = "SELECT "+ COLUMN_PACIENT_ID+" , "+
                    COLUMN_PACIENT_JMENO+" , " +
                    COLUMN_PACIENT_PRIJMENI + " , "+
                    COLUMN_PACIENT_ADRESA + " , " +
                    COLUMN_PACIENT_RC+" , " +
                    COLUMN_PACIENT_CP+ " , "+
                    COLUMN_PACIENT_FLAGS+ " , "+
                    COLUMN_PACIENT_IBOB +" , " +
                    COLUMN_PACIENT_PBI + " , "+
                    COLUMN_PACIENT_DKEY + " , "+
                    COLUMN_PACIENT_PARO+" , " +
                    COLUMN_PACIENT_STONE + ", "+
                    COLUMN_PACIENT_DOKTOR + " , "+
                    COLUMN_PACIENT_TELEFON + " , "+
                    COLUMN_PACIENT_NOTES +" FROM "+TABLE_PACIENTI+" WHERE "+COLUMN_PACIENT_ID+" = '%1'";




            const QString SELECT_PATIENTS_VIEW = "SELECT "+COLUMN_PACIENT_ID+", "+COLUMN_PACIENT_PRIJMENI + ", "+ COLUMN_PACIENT_JMENO +
                     ", " + COLUMN_PACIENT_RC +  ", " + COLUMN_PACIENT_FLAGS +", " + COLUMN_PACIENT_TELEFON + " FROM " + TABLE_PACIENTI;

            const QString SELECT_ZAKROKY_VIEW = "SELECT "+COLUMN_ZAKROKY_ID+", "+COLUMN_ZAKROKY_ZAKROK + ", "+ COLUMN_ZAKROKY_CENA+ " FROM "+ TABLE_ZAKROKY;

            const QString SELECT_PROHLIDKY_PACIENTA_VIEW = " SELECT "+COLUMN_PROHLIDKA_ID+" , "+COLUMN_PROHLIDKA_DATE+
                    " FROM "+ TABLE_PROHLIDKA+" WHERE "+COLUMN_PROHLIDKA_ID_PACIENT+" = '%1'";
            const QString SELECT_PROHLIDKA_VIEW= " SELECT "+ COLUMN_PROHLIDKA_ID +" , "+ COLUMN_PROHLIDKA_DATE+" , "+ COLUMN_PROHLIDKA_ID_ZAKROKY+
                            " FROM "+TABLE_PROHLIDKA+" WHERE "+COLUMN_PROHLIDKA_ID_PACIENT+" = '%1'";

            const QString INSERT_PROHLIDKA = "INSERT INTO "+TABLE_PROHLIDKA+" ( "+COLUMN_PROHLIDKA_DATE+" , "+COLUMN_PROHLIDKA_ID_PACIENT+" , "
                    +COLUMN_PROHLIDKA_NOTES+" , "+ COLUMN_PROHLIDKA_ID_ZAKROKY+" ) values ( '%1','%2','%3','%4')";

            const QString INSERT_ZAKROKY_PROHLIDKY = "INSERT INTO "+TABLE_PROHLIDKA_ZAKROKY+" ( "+COLUMN_PROHLIDKA_ZAKROKY_ID_PROHLIDKA+" , "+
                        COLUMN_PROHLIDKA_ZAKROKY_ID_ZAKROK+" ) values ( '%1','%2')";

            const QString CREATE_PATIENTS = "CREATE TABLE IF NOT EXISTS " + TABLE_PACIENTI + " ( "+COLUMN_PACIENT_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "+
                        COLUMN_PACIENT_JMENO+" TEXT NOT NULL, " +
                        COLUMN_PACIENT_PRIJMENI + " TEXT NOT NULL, "+
                        COLUMN_PACIENT_ADRESA + " TEXT, " +
                        COLUMN_PACIENT_RC+" TEXT  NOT NULL, " +
                        COLUMN_PACIENT_CP+ " TEXT , "+
                        COLUMN_PACIENT_FLAGS+ " TEXT , "+
                        COLUMN_PACIENT_IBOB +" TEXT , " +
                        COLUMN_PACIENT_PBI + " TEXT , "+
                        COLUMN_PACIENT_DKEY + " TEXT , "+
                        COLUMN_PACIENT_PARO+" TEXT , " +
                        COLUMN_PACIENT_STONE + " TEXT, "+
                        COLUMN_PACIENT_DOKTOR + " TEXT, "+
                        COLUMN_PACIENT_TELEFON + " TEXT, "+
                        COLUMN_PACIENT_NOTES +" TEXT ) ;";


            const QString CREATE_PROHLIDKA = "CREATE TABLE IF NOT EXISTS "+TABLE_PROHLIDKA+" ( "+
                                             COLUMN_PROHLIDKA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT , "+
                                             COLUMN_PROHLIDKA_ID_PACIENT + " TEXT NOT NULL , "+
                                             COLUMN_PROHLIDKA_DATE + " TEXT NOT NULL, "+
                                             COLUMN_PROHLIDKA_ID_ZAKROKY + " TEXT, "+
                                             COLUMN_PROHLIDKA_NOTES + " TEXT ) ;";


            const QString CREATE_PROHLIDKA_ZAKROKY = "CREATE TABLE IF NOT EXISTS "+TABLE_PROHLIDKA_ZAKROKY+" ( "+
                                                      COLUMN_PROHLIDKA_ZAKROKY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                                                      COLUMN_PROHLIDKA_ZAKROKY_ID_PROHLIDKA + " INTEGER NOT NULL, "+
                                                      COLUMN_PROHLIDKA_ZAKROKY_ID_ZAKROK + " INTEGER NOT NULL );";
            const QString CREATE_ZAKROKY = "CREATE TABLE IF NOT EXISTS "+TABLE_ZAKROKY+ " ( "+
                                            COLUMN_ZAKROKY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                                            COLUMN_ZAKROKY_ZAKROK + " TEXT NOT NULL, "+
                                            COLUMN_ZAKROKY_CENA + " INTEGER );";

            const QString CREATE_META = "CREATE TABLE IF NOT EXISTS "+TABLE_META+" ( "+
                                        COLUMN_META_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                                        COLUMN_META_TAG + " TEXT NOT NULL, "+
                                        COLUMN_META_CONTENT + " TEXT );";



            DatabaseAgent(DatabaseAgent const&)               = delete;
            void operator=(DatabaseAgent const&)  = delete;



             void insert(QString table,QString sql, QVector<QString> params);
             void update(QString table,QString sql, QVector<QString> params);





};

#endif // DATABASEAGENT_H
