#ifndef TABLEFIELDS_H
#define TABLEFIELDS_H
enum {
    Pacienti_id = 0,
    Pacienti_Prijmeni = 1,
    Pacienti_Jmeno = 2,
    Pacienti_RC = 3,
    Pacienti_Flags = 4,
    Pacienti_Telefon = 5

};
#endif // TABLEFIELDS_H
