#ifndef ZAKROKYWINDOW_H
#define ZAKROKYWINDOW_H

#include <QDialog>
#include <QSqlQueryModel>
#include "databaseagent.h"
#include <QSqlTableModel>
enum {
    Zakroky_id = 0,
    Zakroky_zakrok = 1,
    Zakroky_cena = 2
};


namespace Ui {
class zakrokywindow;
}

class ZakrokyWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ZakrokyWindow(QWidget *parent = 0);
    ~ZakrokyWindow();

private slots:

    void on_buttonAddZakrok_clicked();

    void on_buttonEditZakrok_clicked();

    void on_buttonDeleteZakrok_clicked();




private:
    Ui::zakrokywindow *ui;
     QSqlQueryModel *model;
     void initModel();
     void zakrokyUpdated();
     bool validateZakrok();
};


#endif // ZAKROKYWINDOW_H
