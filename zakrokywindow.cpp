#include "zakrokywindow.h"
#include "ui_zakrokywindow.h"

ZakrokyWindow::ZakrokyWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::zakrokywindow)
{

    DatabaseAgent::getInstance();
    ui->setupUi(this);
    initModel();
}

ZakrokyWindow::~ZakrokyWindow()
{
    delete ui;
}



void ZakrokyWindow::on_buttonAddZakrok_clicked()
{
    if(validateZakrok())
    {
        DatabaseAgent::getInstance().insertZakrok(ui->textZakrok->text(),  ui->textCena->text());
        zakrokyUpdated();
    }else{
       QMessageBox msgBox;
       msgBox.setText("Špatný formát zákroku");
       msgBox.setInformativeText("Zadali jste nevyhovující formát zákroku. Popisek zákroku nesmí být prázdný a cena musí být vyšší než nula.");
       msgBox.setStandardButtons(QMessageBox::Ok);
       msgBox.setDefaultButton(QMessageBox::Ok);
       int ret = msgBox.exec();
       qDebug() << "msgbox finish " <<ret;
    }
}

void ZakrokyWindow::on_buttonEditZakrok_clicked()
{
    if((ui->tableView->currentIndex().isValid())&&(!ui->textCena->text().isEmpty())&&(!ui->textZakrok->text().isEmpty()))
    {
//        int id = ui->tableView->selectionModel()->selectedRows().toVector().first();
//        QString editid = QString::number(id);

        QString editid = model->data(model->index(ui->tableView->selectionModel()->currentIndex().row(),0)).toString();
        qDebug() << "edit id: "<< editid;
        DatabaseAgent::getInstance().upravZakrok(editid,ui->textZakrok->text(), ui->textCena->text());
        zakrokyUpdated();

    }

}

void ZakrokyWindow::on_buttonDeleteZakrok_clicked()
{
    if(ui->tableView->currentIndex().isValid()){
        QString delid = model->data(model->index(ui->tableView->selectionModel()->currentIndex().row(),0)).toString();

        qDebug() << "edit id: "<< delid;
        DatabaseAgent::getInstance().deleteZakrok(delid);
        zakrokyUpdated();

    }
}

void ZakrokyWindow::initModel(){



    //    model =  new QSqlTableModel(ui->tableView, DatabaseAgent::getInstance().db);
        model =  new QSqlTableModel();


        //model->setTable(DatabaseAgent::getInstance().getTABLE_PACIENTI());

        model->setHeaderData(Zakroky_id,Qt::Horizontal, QObject::tr("ID"));
        model->setHeaderData(Zakroky_zakrok,Qt::Horizontal, QObject::tr("Zákrok"));
        model->setHeaderData(Zakroky_cena,Qt::Horizontal, QObject::tr("Cena"));

        model->setQuery(DatabaseAgent::getInstance().getSELECT_ZAKROKY_VIEW());



        ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);
        ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        ui->tableView->setColumnHidden(Zakroky_id, true);
        ui->tableView->resizeColumnsToContents();
        ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);


        ui->tableView->setModel(model);

        QHeaderView *header = ui->tableView->horizontalHeader();
        header->setStretchLastSection(true);

       // QItemSelectionModel *select = ui->tableView->selectionModel();
        //connect(select, SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(rowChanged()));

        ui->tableView->show();
    }


void  ZakrokyWindow::zakrokyUpdated()
{
    qDebug() << "zakrokyUpdated";
        model->setQuery(DatabaseAgent::getInstance().getSELECT_ZAKROKY_VIEW());
        qDebug() << "model updated";

         ui->tableView->setModel(model);
         qDebug() << "model set";

}

bool ZakrokyWindow::validateZakrok()
{
bool ret = false;
qDebug() << "validace zakroku";

    ui->textCena->setText( ui->textCena->text().replace(",","."));
    if((!ui->textZakrok->text().isEmpty())&&((ui->textCena->text().toDouble())>0)) ret = true;      //TODO Fix špatný formát


return ret;
}
