#include "patientwindow.h"
#include "ui_patientwindow.h"
#include "databaseagent.h"
PatientWindow::PatientWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PatientWindow)
{



    ui->setupUi(this);
}






PatientWindow::~PatientWindow()
{
    delete ui;
}

void PatientWindow::populateEdit(QString id)
{
    newSwitch = false;
    pacient p = DatabaseAgent::getInstance().getPacient(id);
    ui->textAdress->insertPlainText(p.adresa);  //ttodo fix tady nic neni
    ui->textSurname->setText(p.prijmeni);
    ui->textBornNumber->setText(p.rodnec);
    ui->textCareTaker->setText(p.doktor);
    ui->textName->setText(p.jmeno);
    ui->textInsuranceNumber->setText(p.cislopojistence);
    ui->textPhone->setText(p.telefon);
    ui->textNotes->setPlainText(p.poznamky);    //todo fix tady je adresa
    ui->labelID->setText(p.id);
    //ui->flagAsthma->setPlainText(p.);
   // ui->flagCardio->setEnabled(false);
   // ui->flagHIVPositive->setEnabled(false);
   // ui->flagOther->setEnabled(false);

}

void PatientWindow::populateEmpty()
{
    newSwitch = true;
}

void PatientWindow::lockFields()
{
    ui->textAdress->setEnabled(false);
    ui->textSurname->setEnabled(false);
    ui->textBornNumber->setEnabled(false);
    ui->textCareTaker->setEnabled(false);
    ui->textName->setEnabled(false);
    ui->textInsuranceNumber->setEnabled(false);
    ui->textPhone->setEnabled(false);
    ui->textNotes->setEnabled(false);
    ui->flagAsthma->setEnabled(false);
    ui->flagCardio->setEnabled(false);
    ui->flagHIVPositive->setEnabled(false);
    ui->flagOther->setEnabled(false);
    ui->buttonSaveChanges->setEnabled(false);
}

void PatientWindow::unlockFields()
{
    ui->textAdress->setEnabled(true);
    ui->textSurname->setEnabled(true);
    ui->textBornNumber->setEnabled(true);
    ui->textCareTaker->setEnabled(true);
    ui->textName->setEnabled(true);
    ui->textInsuranceNumber->setEnabled(true);
    ui->textPhone->setEnabled(true);
    ui->textNotes->setEnabled(true);
    ui->flagAsthma->setEnabled(true);
    ui->flagCardio->setEnabled(true);
    ui->flagHIVPositive->setEnabled(true);
    ui->flagOther->setEnabled(true);
}

void PatientWindow::toggleFields()
{
    if(!ui->textAdress->isEnabled()){
        unlockFields();
    }else{
        lockFields();
    }
}

void PatientWindow::infoChanged()
{
    ui->buttonSaveChanges->setEnabled(true);
}

void PatientWindow::on_buttonSaveChanges_clicked()
{
    if(validateInfo()){
        newSwitch = false;

        QString jmeno = ui->textName->text();
        QString prijmeni = ui->textSurname->text();
        QString telefon = ui->textPhone->text();    //Nic
        QString bornNum = ui->textBornNumber->text();   //Adresa
        QString caretaker = ui->textBornNumber->text();
        QString insuranceNum = ui->textInsuranceNumber->text(); //bornnum
        QString notes = ui->textNotes->toPlainText();
        QString adress = ui->textAdress->toPlainText(); //adresa
        QString flags = "";
        if(ui->flagAsthma->isChecked()) flags += "a";
        if(ui->flagCardio->isChecked()) flags += "k";
        if(ui->flagHIVPositive->isChecked()) flags += "h";
        if(ui->flagOther->isChecked()) flags += "j";

        DatabaseAgent::getInstance().insertNewPatient(jmeno,prijmeni,bornNum, adress, insuranceNum, telefon, caretaker, flags, notes);
        emit patientAdded();

    }
}

bool PatientWindow::validateInfo()
{
//TODO validace zadaných dat
    return true;
}

void PatientWindow::on_buttonFinishAppointment_clicked()
{
    //TODO Ukončení prohlídky
    qDebug() << "ukonceni prohlidky";

    QString zakId = QString(DatabaseAgent::getInstance().insertNewProhlidka(ui->appointDate->text(),ui->labelID->text(),ui->notes->toPlainText(),0));
    qDebug() << "insert";


    DatabaseAgent::getInstance().insertZakrokyProhlidky( *basket,zakId);

}

void PatientWindow::on_buttonClearNotes_clicked()
{
    ui->notes->clear();
}

void PatientWindow::on_buttonNewPatient_clicked()
{

}

void PatientWindow::on_buttonEditPatient_clicked()
{
    //unlockFields();
}

void PatientWindow::on_buttonArchivePatient_clicked()
{

}

void PatientWindow::on_buttonPreviousPatient_clicked()
{
    int id =ui->labelID->text().toInt();
    if(id>0) id-=1;
    else{
        QSqlQuery query;
        query.exec("SELECT COUNT(*) FROM "+DatabaseAgent::getInstance().TABLE_PACIENTI);

    }
    populateEdit("-1");
}

void PatientWindow::on_buttonNextPatient_clicked()
{

}

void PatientWindow::on_buttonClose_clicked()
{

}

void PatientWindow::on_buttonAddToBasket_clicked()
{
    if(ui->availableZakrokyList->selectionModel()->currentIndex().isValid())
    {
        QModelIndexList selectedIds = ui->availableZakrokyList->selectionModel()->selectedIndexes();

           int id = selectedIds.at(0).row();

        qDebug() << "Basket size: " << basket->size();
        basket->append(QString::number(id));
        qDebug() << "Basket size: " << basket->size();
    }

}

void PatientWindow::on_buttonRemoveFromBasket_clicked()
{

}

void PatientWindow::on_buttonPrintBasket_clicked()
{

}

void PatientWindow::on_tabWidget_tabBarClicked(int index)
{
    switch(index){
    case 1:

        refreshLists(QString::number(currentId)); break;
    }
}

void PatientWindow::refreshLists(QString patientId)
{
    basket = new QStringList;

    appointModel = new QStringListModel(this);
    basketModel= new QStringListModel(this);
    zakrokModel= new QStringListModel(this);
    QStringList appointIds;
    QStringList appointDates;
    QStringList zakrokyId;
    QStringList zakrokyDescript;

    QSqlQuery queryapp;
    QSqlQuery queryzak;

    queryapp.exec(DatabaseAgent::getInstance().getSELECT_PROHLIDKY_VIEW(patientId));
    while(queryapp.next()){
        QSqlRecord record = queryapp.record();
        qDebug() << "appointment: " << record.value(DatabaseAgent::getInstance().COLUMN_PROHLIDKA_ID)<< " from date " << record.value(DatabaseAgent::getInstance().COLUMN_PROHLIDKA_DATE);
        appointIds.append(record.value(DatabaseAgent::getInstance().COLUMN_PROHLIDKA_ID).toString());
        appointDates.append(record.value(DatabaseAgent::getInstance().COLUMN_PROHLIDKA_DATE).toString());
    }
    appointModel->setStringList((QStringList&) appointDates);
    ui->listExaminations->setModel(appointModel);

    queryzak.exec(DatabaseAgent::getInstance().getSELECT_ZAKROKY_VIEW());

    zakrokyDescript.reserve(queryzak.size()*sizeof(QString));

    zakrokyId.reserve(queryzak.size()*sizeof(QString));

    while(queryzak.next()){
        QSqlRecord record = queryzak.record();
        qDebug() << "zakroky: " << record.value(DatabaseAgent::getInstance().COLUMN_ZAKROKY_ID).toString()<< " called: "
                 << record.value(DatabaseAgent::getInstance().COLUMN_ZAKROKY_ZAKROK).toString() << " with price: "
                 << record.value(DatabaseAgent::getInstance().COLUMN_ZAKROKY_CENA).toString();
        zakrokyId.append(record.value(DatabaseAgent::getInstance().COLUMN_ZAKROKY_ID).toString());
        zakrokyDescript.append( record.value(DatabaseAgent::getInstance().COLUMN_ZAKROKY_ZAKROK).toString() + "  -  "+
                                 (record.value(DatabaseAgent::getInstance().COLUMN_ZAKROKY_CENA).toString())+" Kč");
    }

    zakrokModel->setStringList(zakrokyDescript);
    ui->availableZakrokyList->setModel(zakrokModel);
    basketModel->setStringList(*basket);
    //basketModel->setStringList("");
    ui->basketList->setModel(basketModel);



}

void PatientWindow::on_pushButton_clicked()
{

}

void PatientWindow::newAppointment(QString idPatient)
{
   populateEdit(idPatient);
   ui->tabWidget->setCurrentIndex(1);
   refreshLists(idPatient);

   this->show();
}

void PatientWindow::updatePacient(pacient p)
{
    *currentPacient = p;
}

pacient PatientWindow::getCurrentPacient()
{
    return *currentPacient;
}

QString PatientWindow::getCurrPacId()
{
    return currentPacient->id;
}

void PatientWindow::setCurrentId(int id)
{
    currentId = id;
}
