#include "databaseagent.h"

void DatabaseAgent::createTables(){
    qDebug() << "Creating tables" ;

    QSqlQuery query;
    qDebug() << "Creating patients..." ;
    qDebug() << CREATE_PATIENTS;
    query.exec(CREATE_PATIENTS);
    qDebug() <<  query.lastError().text() ;
    query.clear();
    qDebug() << "Creating appointments..." ;
    qDebug() << CREATE_PROHLIDKA;
    query.exec(CREATE_PROHLIDKA);
    qDebug() <<  query.lastError().text() ;
    query.clear();
    qDebug() << "Creating appointments-acts..." ;
    qDebug() << CREATE_PROHLIDKA_ZAKROKY;
    query.exec(CREATE_PROHLIDKA_ZAKROKY);
    qDebug() <<  query.lastError().text() ;
    query.clear();
    qDebug() << "Creating acts..." ;
    qDebug() << CREATE_ZAKROKY;
    query.exec(CREATE_ZAKROKY);
    qDebug() <<  query.lastError().text() ;
    query.clear();
    qDebug() << "Creating meta..." ;
    qDebug() << CREATE_META;
    query.exec(CREATE_META);
    qDebug() <<  query.lastError().text() ;
    query.clear();


}

QString DatabaseAgent::getSELECT_PATIENTS_VIEW() const
{
    return SELECT_PATIENTS_VIEW;
}

QString DatabaseAgent::getSELECT_ZAKROKY_VIEW() const
{
    return SELECT_ZAKROKY_VIEW;
}


void DatabaseAgent::insert(QString table,QString sql, QVector<QString> params)
{
    //TODO insert
}


void DatabaseAgent::insertZakrok(QString zakrok, QString cena){
    //todo insert zákrok, delegovat přes insert(...)
    qDebug() << "cena : "+cena;
    QString sql = "insert into "+TABLE_ZAKROKY+" ( "+COLUMN_ZAKROKY_ZAKROK
                        +", "+COLUMN_ZAKROKY_CENA+" ) values ( '"+ zakrok+"','"+ cena+"')";
    QSqlQuery query;
    query.exec(sql);
    qDebug() << "Insert: " << query.lastQuery();
    qDebug() << "Error: " << query.lastError().text();


}

void DatabaseAgent::deleteZakrok(QString id){
    //todo delete zákrok, delegovat přes delete(...)

    QString delql = "delete from "+TABLE_ZAKROKY+" where "+COLUMN_ZAKROKY_ID+" = "+id;
    QSqlQuery query;
    query.exec(delql);
    qDebug() << "Delete: " << query.lastQuery();
    qDebug() << "Error: " << query.lastError().text();


}

void DatabaseAgent::upravZakrok(QString id, QString zakrok, QString cena){
//todo update zákrok, delegovat přes update(...)
    QString updtsql = "update "+TABLE_ZAKROKY+
            " set "+COLUMN_ZAKROKY_ZAKROK+" = '"+ zakrok+
                   +"' , "+ COLUMN_ZAKROKY_CENA+" = '"+ cena+
                    "' where "+COLUMN_ZAKROKY_ID+" = "+id;
    QSqlQuery query;
    query.exec(updtsql);
    qDebug() << "Update: " << query.lastQuery();
    qDebug() << "Error: " << query.lastError().text();

}


 void DatabaseAgent::update(QString table,QString sql, QVector<QString> params)
{
    //TODO update
}

 QString DatabaseAgent::getTABLE_PACIENTI() const
 {
     return TABLE_PACIENTI;
 }

 QString DatabaseAgent::getTABLE_PROHLIDKA() const
 {
     return TABLE_PROHLIDKA;
 }

 QString DatabaseAgent::getTABLE_PROHLIDKA_ZAKROKY() const
 {
     return TABLE_PROHLIDKA_ZAKROKY;
 }

 QString DatabaseAgent::getTABLE_ZAKROKY() const
 {
     return TABLE_ZAKROKY;
 }

 QString DatabaseAgent::getTABLE_META() const
 {
     return TABLE_META;
 }

 void DatabaseAgent::archivatePatient(int id)
 {
     qDebug() << "archivuji" << QString::number(id);
    //TODO archivace
}

// void DatabaseAgent::insertNewPatient(QString name, QString surname, QString dateOfBirth, QString bornNum, QString adress, QString insuranceNum)
// {
//     insertNewPatient(name,surname, dateOfBirth,bornNum,insuranceNum,"","","","");

// }

 void DatabaseAgent::insertNewPatient(QString name, QString surname,
                                       QString bornNum, QString adress,QString insuranceNum, QString phone,
                                      QString caretaker, QString flags, QString notes)
 {
  QSqlQuery query;

    query.exec("INSERT INTO "+TABLE_PACIENTI+" ( "+
                  COLUMN_PACIENT_JMENO + ", " +
                  COLUMN_PACIENT_PRIJMENI + ", " +
                  COLUMN_PACIENT_RC + ", " +
                  COLUMN_PACIENT_CP + ", " +
                  COLUMN_PACIENT_ADRESA + ", " +
                  COLUMN_PACIENT_DOKTOR + ", " +
                  COLUMN_PACIENT_TELEFON + ", " +
                  COLUMN_PACIENT_FLAGS + ", " +
                  COLUMN_PACIENT_NOTES + " ) "+
                  " values ( '"+
                   name+"','"+ surname+"','"+ bornNum+"','"+ insuranceNum+"','"+
                  adress+"','"+caretaker+"','"+phone+"','"+flags+"','"+notes+"')");
    qDebug() << "Insert: " << query.lastQuery();
    qDebug() << "Error: " << query.lastError().text();

 }

 int DatabaseAgent::insertNewProhlidka(QString date, QString idPacient, QString notes, QString idZakroky)
 {
     QSqlQuery query;
     query.exec(getINSERT_PROHLIDKA(date,idPacient,notes,idZakroky));
     qDebug() << "Insert: " << query.lastQuery();
     qDebug() << "Error: " << query.lastError().text();
     int zakId = query.lastInsertId().toInt();
            qDebug()<<" returning " << zakId;
     return zakId;


 }





 void DatabaseAgent::insertTestData()
 {
     insertNewPatient("Alfred","Brunátný","665595/5462","Podmokelská 32, Děčín","15464612","","","aj","");
 }
 
 QString DatabaseAgent::getSELECT_PACIENT(QString pacientId) const
 {
     return SELECT_PACIENT.arg(pacientId);
 }
 
 QString DatabaseAgent::getINSERT_ZAKROKY_PROHLIDKY(QString idProhlidka, QString idZakrok)
 {
     return INSERT_ZAKROKY_PROHLIDKY.arg(idProhlidka).arg(idZakrok);
 }

 QString DatabaseAgent::getINSERT_PROHLIDKA(QString date, QString idPacient, QString notes, QString idZakroky)
 {
     return INSERT_PROHLIDKA.arg(date).arg(idPacient).arg(notes).arg(idZakroky);
 }

 QString DatabaseAgent::getSELECT_PROHLIDKA_VIEW(QString patientId)
 {
     return SELECT_PROHLIDKA_VIEW.arg(patientId);
 }

 QString DatabaseAgent::getSELECT_PROHLIDKY_VIEW(QString patientId)
 {

     QString ret;
     ret = SELECT_PROHLIDKY_PACIENTA_VIEW;
     return ret.arg(patientId);
 }

 void DatabaseAgent::insertZakrokyProhlidky(QStringList zakroky, QString idprohlidky)
 {
     QSqlQuery query;
     QStringList::iterator i;
     int c = 0;
     for(  i = zakroky.begin(); i<zakroky.end();++i){
         query.exec(getINSERT_ZAKROKY_PROHLIDKY(idprohlidky, zakroky.first().at(0)));
         qDebug() << "Insert "<< c<<": " << query.lastQuery();
         qDebug() << "Error: " << query.lastError().text();
         zakroky.removeFirst();
     }
 }



 void DatabaseAgent::getProhlidky(QString patientId)
 {

 }

 void DatabaseAgent::getProhlidka(QString prohlidkaId){

 }

 pacient DatabaseAgent::getPacient(QString pacientId){
     QSqlQuery query;
     query.exec(getSELECT_PACIENT(pacientId));
     query.next();
     pacient p;
        p.id = query.value(0).toString();
          p.jmeno = query.value(1).toString();
          p.prijmeni = query.value(2).toString();
          p.adresa = query.value(3).toString();
          p.rodnec = query.value(4).toString();
          p.cislopojistence = query.value(5).toString();
          p.znacky = query.value(6).toString();
          p.ibob = query.value(7).toString();
          p.pbi = query.value(8).toString();
          p.zubniklic = query.value(9).toString();
          p.parodontoza = query.value(10).toString();
          p.zubnikamen = query.value(11).toString();
          p.doktor = query.value(12).toString();
          p.telefon = query.value(13).toString();
          p.poznamky = query.value(14).toString();

     return p;
 }



