#ifndef PATIENTWINDOW_H
#define PATIENTWINDOW_H

#include <QDialog>
#include <QStringListModel>
#include <mainwindow.h>
#include "pacient.h"
namespace Ui {

class MainScreen;

class PatientWindow;
}

class PatientWindow : public QDialog
{
    Q_OBJECT

public:
    explicit PatientWindow(QWidget *parent = 0);
    void setCurrentId(int id);
private:
    Ui::PatientWindow *ui;
    MainWindow *mw;
    ~PatientWindow();
    QStringListModel *appointModel;
    QStringListModel *zakrokModel;
    QStringListModel *basketModel;
    QStringList *basket;
    QStringList *zakrokIds;
    pacient *currentPacient;
    bool newSwitch;
    int currentId;

public slots:
    void toggleFields();
    void lockFields();
    void unlockFields();
    void populateEmpty();
    void populateEdit(QString id);
    bool validateInfo();
    void refreshLists(QString patientId);
    void newAppointment(QString idPatient);
    void updatePacient(pacient p);
    pacient getCurrentPacient();
    QString getCurrPacId();
private slots:
    void infoChanged();


    void on_buttonSaveChanges_clicked();

    void on_buttonFinishAppointment_clicked();

    void on_buttonClearNotes_clicked();

    void on_buttonNewPatient_clicked();

    void on_buttonEditPatient_clicked();

    void on_buttonArchivePatient_clicked();

    void on_buttonPreviousPatient_clicked();

    void on_buttonNextPatient_clicked();

    void on_buttonClose_clicked();

    void on_buttonAddToBasket_clicked();

    void on_buttonRemoveFromBasket_clicked();

    void on_buttonPrintBasket_clicked();

    void on_tabWidget_tabBarClicked(int index);

    void on_pushButton_clicked();

signals:
    patientAdded();

};

#endif // PATIENTWINDOW_H
