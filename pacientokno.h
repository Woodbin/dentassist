#ifndef PACIENTOKNO_H
#define PACIENTOKNO_H

#include <QDialog>

namespace Ui {
class PacientOkno;
}

class PacientOkno : public QDialog
{
    Q_OBJECT

public:
    explicit PacientOkno(QWidget *parent = 0);
    ~PacientOkno();

private:
    Ui::PacientOkno *ui;
};

#endif // PACIENTOKNO_H
