#-------------------------------------------------
#
# Project created by QtCreator 2015-10-28T21:05:34
#
#-------------------------------------------------

QT       += core gui sql


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DentAssist
TEMPLATE = app


CONFIG +=c++14

SOURCES += main.cpp\
    logindialog.cpp \
    patientwindow.cpp \
    databaseagent.cpp \
    mainscreen.cpp \
    zakrokywindow.cpp \
    pacient.cpp

HEADERS  += \
    logindialog.h \
    patientwindow.h \
    databaseagent.h \
    mainscreen.h \
    tablefields.h \
    zakrokywindow.h \
    pacient.h \
    zubniklic.h

FORMS    += mainwindow.ui \
    logindialog.ui \
    patientwindow.ui \
    mainscreen.ui \
    zakrokywindow.ui
